cp helpers/img/kobologo.svg /opt/backend-install/.vols/static/kobocat/images/kobologo.svg
cp helpers/img/kobologo.svg /opt/backend-install/.vols/static/kpi/compiled/kobologo.svg
cp helpers/img/kobologo.svg /opt/backend-install/.vols/static/kpi/img/kobologo.svg
cp helpers/img/kobologo.svg /opt/backend-install/nginx/maintenance/www/kobologo.svg
cp helpers/img/signup_photo.jpg /opt/backend-install/.vols/static/kpi/compiled/signup_photo.jpg
cp helpers/img/signup_photo.jpg /opt/backend-install/.vols/static/kpi/img/signup_photo.jpg
cp helpers/img/favicon.ico /opt/backend-install/.vols/static/kobocat/images/favicon.ico
cp helpers/img/favicon.ico /opt/backend-install/.vols/static/kpi/favicon-16x16.png
cp helpers/img/favicon.ico /opt/backend-install/.vols/static/kpi/favicon-32x32.png
cp helpers/img/favicon.ico /opt/backend-install/.vols/static/kpi/favicon.ico
cp helpers/img/favicon.ico /opt/backend-install/.vols/static/kpi/img/favicon.ico
cp helpers/img/favicon.ico /opt/backend-install/.vols/static/kpi/rest_framework/docs/img/favicon.ico
cp helpers/img/favicon.ico /opt/backend-install/enketo_express/favicon.ico
cp helpers/img/favicon.ico /opt/backend-install/nginx/maintenance/www/favicon.ico
cp helpers/img/favicon.ico /usr/share/gitweb/static/git-favicon.png




