echo "Input action: (setup|restart|start|stop): "
read INPUT_STRING
case $INPUT_STRING in
    setup)
	apt update
	apt upgrade -y
	apt install git htop -y
	apt-get remove docker docker-engine docker.io containerd runc
	apt-get install \
		apt-transport-https \
		ca-certificates \
		curl \
		gnupg-agent \
		software-properties-common -y

	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	apt-key fingerprint 0EBFCD88
	add-apt-repository \
	    "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"	 
	apt-get update
	apt-get install docker-ce docker-ce-cli containerd.io -y
	docker run hello-world
	apt autoremove -y
	curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	chmod +x /usr/local/bin/docker-compose
	ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
	python3 run.py
	sh sync.sh
	break
	;;
    start)
	python3 run.py
	sh sync.sh
	break
	;;
    stop)
	python3 run.py --stop
	break
	;;
    restart)
	python3 run.py --stop
	python3 run.py
	sh sync.sh
	break
	;;
    *)
	echo "The input can't be reconigzed, rerun the script and try again."
	;;
esac
